package show;

import com.google.gson.Gson;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.Arrays;
import java.util.Base64;
import java.util.Properties;

/**
 * @author chenqh
 * @version 1.0.0
 * @date 2020-07-13
 */
public class ReadFromKafkaAndShowInUI {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    /**
     * 从JSON数据解析出byte数组
     *
     * @param imageJson Base64编码的JSON数据
     * @return 解析后的二进制数据
     */
    public static VideoData getByteFromJson(String imageJson) {
//        String input = new String(imageJson, "UTF-8");
        Gson gson = new Gson();
        VideoData videoData = gson.fromJson(imageJson, VideoData.class);
        return videoData;
    }

    /**
     *
     */
    public static void showPicByOpenCV(VideoData videoData) throws Exception {

        Mat frame = getMatFromJpgByte(videoData.rows, videoData.cols, videoData.type, Base64.getDecoder().decode(videoData.data));
//        Imgcodecs.imwrite("E:\\003.jpg", frame);
        HighGui.imshow("show", frame);
        HighGui.waitKey(1);
    }
    /**
     * 从Jpg字节流中生成Mat对象
     * */
    private static Mat getMatFromJpgByte(int row, int col, int type, byte[] data) throws Exception {
        MatOfByte matOfByte = new MatOfByte(data);
        Mat mat = Imgcodecs.imdecode(matOfByte, Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);
//        mat.put(0, 0, data);
        return mat;
    }


    public static void main(String[] args) throws Exception {
        // kafka 配置
        Properties props = new Properties();
        props.put("bootstrap.servers", "10.11.1.193:9092");
        props.put("group.id", "SCsxsxxxasx212");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("max.poll.records", 1000);
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", StringDeserializer.class.getName());
        props.put("value.deserializer", StringDeserializer.class.getName());
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
//      方式1
//      consumer.subscribe(Arrays.asList("fps20"));
//      方式2开始
        TopicPartition p = new TopicPartition("app2output", 0);
        consumer.assign(Arrays.asList(p));
        consumer.seek(p, 0);
//      方式2结束
        try {
            while (true) {
                ConsumerRecords<String, String> msgList = consumer.poll(1000);
                System.out.println(msgList.count());
                for (ConsumerRecord<String, String> record : msgList) {
                    System.out.println(record.key());
                    showPicByOpenCV(getByteFromJson(record.value()));
                }
            }
        }
        finally {
            consumer.close();
        }


//        properties.put("partitioner.class", "collector.MyPartitioner");
    }
}
