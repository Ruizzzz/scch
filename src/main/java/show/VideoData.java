package show;

/**
 * @author chenqh
 * @version 1.0.0
 * @date 2020-07-13
 */
public class VideoData {
    String cameraId;
    String timestamp;
    int rows;
    int cols;
    int type;
    String data;

    public int getCols() {
        return cols;
    }

    public int getType() {
        return type;
    }

    public String getCameraId() {
        return cameraId;
    }

    public int getRows() {
        return rows;
    }

    public String getData() {
        return data;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    public void setCols(int cols) {
        this.cols = cols;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setType(int type) {
        this.type = type;
    }
}
