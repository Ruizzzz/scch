package collector;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.*;
import org.apache.log4j.Logger;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * @author chenqh
 * @version 1.0.0
 * @date 2020-01-11
 */
public class ReadFromPicFileWriteToKafka {
    private static final Logger logger = Logger.getLogger(ReadFromPicFileWriteToKafka.class);
    private static final String TWO = "two";
    private static final String THREE = "three";

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) throws Exception {

        // kafka 配置
        Properties prop = PropertyFileReader.readPropertyFile();
        Properties properties = new Properties();
        properties.put("bootstrap.servers", prop.getProperty("kafka.bootstrap.servers"));
        properties.put("acks", prop.getProperty("kafka.acks"));
        properties.put("retries", prop.getProperty("kafka.retries"));
        properties.put("batch.size", prop.getProperty("kafka.batch.size"));
        properties.put("linger.ms", prop.getProperty("kafka.linger.ms"));
        properties.put("max.request.size", prop.getProperty("kafka.max.request.size"));
        properties.put("compression.type", prop.getProperty("kafka.compression.type"));
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("partitioner.class", "collector.MyPartitioner");
        Producer<String, String> producer = new KafkaProducer<String, String>(properties);
        // 遍历文件读取每一张图片写入kafka，kafka共有三个分区
        Map<String, String> fileTopic = new HashMap<>();
        fileTopic.put("/home/spark/tools/three/images","lpr2pic");
//        fileTopic.put("800M", "");
//        fileTopic.put("2G", "");
//        fileTopic.put("6G", "");
//        fileTopic.put("19G", "");

        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        for (Map.Entry<String, String> filetopic : fileTopic.entrySet()) {
            cachedThreadPool.execute(new Runnable() {
                @Override
                public void run() {
                    File file = new File(filetopic.getKey());
                    File[] fs = file.listFiles();
                    assert fs != null;
                    int count = 0;
                    ReadFromPicFileWriteToKafka writeToKafka = new ReadFromPicFileWriteToKafka();
                    for (File f : fs) {
                        if (!f.isDirectory()) {
                            try {
                                count++;
                                writeToKafka.writeToKafka(f.getAbsolutePath().replaceAll("\\\\", "/"), producer, count, filetopic.getValue());
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (count > 10) {
                            break;
                        }
                    }
                    System.out.println("共有" + count + "张图片");
                }
            });
        }

        cachedThreadPool.shutdown();


    }

    private void writeToKafka(String path, Producer<String, String> producer, int count, String topic) throws InterruptedException {
        String id = "partition" + count % 3;
//        Thread.sleep(1000);

        Gson gson = new Gson();
        Mat image = new Mat();
        image = Imgcodecs.imread(path);
        System.out.println(image.total() * image.channels());
        Imgproc.resize(image, image, new Size(640, 480), 0, 0, Imgproc.INTER_CUBIC);

        int cols = image.cols();
        int rows = image.rows();
        int type = image.type();
        // mat to data
        byte[] data = new byte[(int) (image.total() * image.channels())];

        image.get(0, 0, data);
        String timestamp = new Timestamp(System.currentTimeMillis()).toString();
        JsonObject obj = new JsonObject();
        obj.addProperty("cameraId", id);
        obj.addProperty("timestamp", timestamp);
        obj.addProperty("rows", rows);
        obj.addProperty("cols", cols);
        obj.addProperty("type", type);
        obj.addProperty("data", Base64.getEncoder().encodeToString(data));
        String json = gson.toJson(obj);
        producer.send(new ProducerRecord<>(topic, id, json), new EventGeneratorCallback(id));
        logger.info("Generated events for cameraId=" + id + " timestamp=" + timestamp + "count:" + count);
//        System.out.println(json.length() + ":" + id);
        //      System.out.println("===============================");
        //System.out.println(json);
    }

    private class EventGeneratorCallback implements Callback {
        private String camId;

        public EventGeneratorCallback(String camId) {
            super();
            this.camId = camId;
        }

        @Override
        public void onCompletion(RecordMetadata rm, Exception e) {
            if (rm != null) {
                logger.info("cameraId=" + camId + " partition=" + rm.partition());
            }
            if (e != null) {
                e.printStackTrace();
            }
        }
    }

}
