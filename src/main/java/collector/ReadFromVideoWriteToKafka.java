package collector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.log4j.Logger;


/**
 * Class to configure Kafka Producer and connect to Video camera url.
 *
 * @author abaghel
 */
public class ReadFromVideoWriteToKafka {

    private static final Logger logger = Logger.getLogger(ReadFromVideoWriteToKafka.class);
    // 0 topic 1 url 2 fps
    public static void main(String[] args) throws Exception {

        // set producer properties
        Properties prop = PropertyFileReader.readPropertyFile("stream-collector.properties");
        Properties properties = new Properties();
        properties.put("bootstrap.servers", prop.getProperty("kafka.bootstrap.servers"));
        properties.put("acks", prop.getProperty("kafka.acks"));
        properties.put("retries", prop.getProperty("kafka.retries"));
        properties.put("batch.size", prop.getProperty("kafka.batch.size"));
        properties.put("linger.ms", prop.getProperty("kafka.linger.ms"));
        properties.put("max.request.size", prop.getProperty("kafka.max.request.size"));
        properties.put("compression.type", prop.getProperty("kafka.compression.type"));
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("partitioner.class", "collector.MyPartitioner");
        // generate event
        Producer<String, String> producer = new KafkaProducer<String, String>(properties);
        generateIoTEvent(producer, args[0], prop.getProperty("camera.id"), prop.getProperty("camera.url"), Long.parseLong(args[2]), Long.parseLong(args[3]));
    }

    private static void generateIoTEvent(Producer<String, String> producer, String topic, String camId, String videoUrl, long fps, long countNum) throws Exception {
        String[] urls = videoUrl.split(",");
        String[] ids = camId.split(",");
        if (urls.length > ids.length) {
            throw new Exception("url number should < ids number");
        }
        logger.info("Total urls to process " + urls.length);
        for (int i = 0; i < urls.length; i++) {
            Thread t = new Thread(new VideoEventGenerator(ids[i].trim(), urls[i].trim(), producer, topic, fps, countNum));
            t.start();
        }
    }
}
