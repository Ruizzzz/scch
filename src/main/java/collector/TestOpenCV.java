package collector;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

/**
 * @author chenqh
 * @version 1.0.0
 * @date 2020-01-15
 */
public class TestOpenCV {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
    public static void main(String[] args) {
        Mat image = new Mat();
        image = Imgcodecs.imread("E:/zhs/scch/src/main/resources/000001.jpg");
        System.out.println(image.total() * image.channels());
    }
}
