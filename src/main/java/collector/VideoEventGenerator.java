package collector;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Logger;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import java.sql.Timestamp;
import java.util.Base64;

import static java.lang.Thread.sleep;
import static org.opencv.imgcodecs.Imgcodecs.imread;

/**
 * Class to convert Video Frame into byte array and generate JSON event using Kafka Producer.
 *
 * @author abaghel
 */
public class VideoEventGenerator implements Runnable {
    private static final Logger logger = Logger.getLogger(VideoEventGenerator.class);
    private String cameraId;
    private String url;
    private Producer<String, String> producer;
    private String topic;
    private long fps;
    private long countNum = Long.MAX_VALUE;

    public VideoEventGenerator(String cameraId, String url, Producer<String, String> producer, String topic, long fps) {
        this.cameraId = cameraId;
        this.url = url;
        this.producer = producer;
        this.topic = topic;
        this.fps = fps;
    }
    public VideoEventGenerator(String cameraId, String url, Producer<String, String> producer, String topic, long fps, long countNum) {
        this.cameraId = cameraId;
        this.url = url;
        this.producer = producer;
        this.topic = topic;
        this.fps = fps;
        this.countNum = countNum;
    }

    //load OpenCV native lib
    static {
        System.out.println("java version :" + System.getProperty("java.version"));
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("java path: " + System.getProperty("java.library.path"));
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//		System.load("D:\\research\\Video_stream\\video-stream-classification-master\\video-stream-collector\\lib\\opencv\\opencv_ffmpeg320_64.dll");
    }

    @Override
    public void run() {
        logger.info("Processing cameraId " + cameraId + " with url " + url);
        try {
            generateEvent(cameraId, url, producer, topic, fps);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    //generate JSON events for frame
    /**
     * @author chenqh
     * 为了符合JavaFX的Image类的需求，写入的数据均转换为Jpg格式的byte数组
     * */
    private void generateEvent(String cameraId, String url, Producer<String, String> producer, String topic, long fps) throws Exception {
        VideoCapture camera = null;
        if (StringUtils.isNumeric(url)) {
            camera = new VideoCapture(Integer.parseInt(url));
        } else {
            camera = new VideoCapture(url);
        }
        //check camera working
        if (!camera.isOpened()) {
            sleep(5000);
            if (!camera.isOpened()) {
                throw new Exception("Error opening cameraId " + cameraId + " with url=" + url + ".Set correct file path or url in camera.url key of property file.");
            }
        }
        Mat mat = imread(url);
        Gson gson = new Gson();
        int count = 0;
        while (camera.read(mat)) {
            if (count == countNum) {
                break;
            }
//            sleep(50);
            //resize image before sending
            Imgproc.resize(mat, mat, new Size(640, 480), 0, 0, Imgproc.INTER_CUBIC);
            count ++;
            int cols = mat.cols();
            int rows = mat.rows();
            int type = mat.type();
//            byte[] data = new byte[(int) (mat.total() * mat.channels())];
            MatOfByte mob = new MatOfByte();
            Imgcodecs.imencode(".jpg", mat, mob);
            byte[] data = mob.toArray();

            String timestamp = new Timestamp(System.currentTimeMillis()).toString();
            JsonObject obj = new JsonObject();
            obj.addProperty("cameraId", cameraId);
            obj.addProperty("timestamp", timestamp);
            obj.addProperty("rows", rows);
            obj.addProperty("cols", cols);
            obj.addProperty("type", type);

            // jpg格式的byte -> String
            obj.addProperty("data", Base64.getEncoder().encodeToString(data));
            String json = gson.toJson(obj);
            producer.send(new ProducerRecord<String, String>(topic, cameraId, json), new EventGeneratorCallback(cameraId));
            logger.info("Generated events for cameraId=" + cameraId + " timestamp=" + timestamp);
            sleep(1000L / fps);
//            if (count == 450) {
//                break;
//            }
        }
        camera.release();
        mat.release();
    }
    /**
     * 回调函数，确认数据已经成功写入kafka
     * */
    private class EventGeneratorCallback implements Callback {
        private String camId;

        public EventGeneratorCallback(String camId) {
            super();
            this.camId = camId;
        }

        @Override
        public void onCompletion(RecordMetadata rm, Exception e) {
            if (rm != null) {
                logger.info("cameraId=" + camId + " partition=" + rm.partition());
            }
            if (e != null) {
                e.printStackTrace();
            }
        }
    }

}
